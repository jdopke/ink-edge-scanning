## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

# import cv2

import numpy as np
import struct
import os
import sys
import tifffile
from DataReader import readData

if (len(sys.argv) > 3):
    signal = readData(str(sys.argv[1]))
    flat = readData(str(sys.argv[2]))
    dark = readData(str(sys.argv[3]))
    img = (signal-dark)/(flat-dark)
    newName = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+"_ffc.tif"
    newNameReduced = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+"_ffc_reduced.tif"
    print("Saving into "+newName)
    tifffile.imwrite(newName, img)
    tifffile.imwrite(newNameReduced, img.astype(np.float32))
    sys.exit()

if (len(sys.argv) < 2):
    print("Need at least one source file to work with: "+str(sys.argv[0])+" <IMAGEFILE>")
    sys.exit()

print("Converting single source file. To correct Flatfield/Darkfield call: "+str(sys.argv[0])+" <SIGNAL> <FLAT> <DARK>")
signal = readData(str(sys.argv[1]))
newName = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+".tif"
if (len(sys.argv) == 3):
    flat = readData(str(sys.argv[2]))
    newName = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+"_ffc.tif"
    img = (signal)/(flat)
#    print("Normalising/Log")
#    img = np.log((img+1))
#    img = (img-np.min(img))/(np.max(img)-np.min(img))
print("Image dimensions are: "+str(img.shape))
print("Saving into "+newName)
tifffile.imwrite(newName, img.astype(np.float32))

