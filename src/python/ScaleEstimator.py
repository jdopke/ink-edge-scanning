## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

# import cv2

import numpy as np
import struct
import os
import sys
import tifffile
from DataReader import readData

#Region definitions
Alpha = [300,150,400,300]
Beta = [640,150,750,270]
Beta_3 = [1040,140,1140,270]

Background = [900,150,1000,270]

if (len(sys.argv) < 3):
	print("Need the source files as an argument: "+str(sys.argv[0])+" <SIGNAL> <FLAT>")
	sys.exit()

dataOnly = False

if (len(sys.argv) >= 4):
    dataOnly = True
    
high = readData(sys.argv[1])
print("Signal Min: "+str(np.min(high))+"\tMax: "+str(np.max(high)))
low = readData(sys.argv[2])
print("Flat Min: "+str(np.min(low))+"\tMax: "+str(np.max(low)))

img = low/high

if (dataOnly):
    filename = os.path.basename(sys.argv[1])
    Energy = int(filename.split("_")[1])
    Energy = Energy + float(filename.split("_")[2])/1000.

    print(str(Energy)+"\t"+
          str(np.mean(img[Background[1]:Background[3],Background[0]:Background[2]]))+"\t"+
          str(np.std(img[Background[1]:Background[3],Background[0]:Background[2]])) +"\t"+
          str(np.mean(img[Alpha[1]:Alpha[3],Alpha[0]:Alpha[2]]))                    +"\t"+
          str(np.std(img[Alpha[1]:Alpha[3],Alpha[0]:Alpha[2]]))                     +"\t"+
          str(np.mean(img[Beta[1]:Beta[3],Beta[0]:Beta[2]]))                        +"\t"+
          str(np.std(img[Beta[1]:Beta[3],Beta[0]:Beta[2]]))                         +"\t"+
          str(np.mean(img[Beta_3[1]:Beta_3[3],Beta_3[0]:Beta_3[2]]))                +"\t"+
          str(np.std(img[Beta_3[1]:Beta_3[3],Beta_3[0]:Beta_3[2]])))
    sys.exit()

print("Min: "+str(np.min(img))+"\tMax: "+str(np.max(img)))

newName = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+"_div.tif"
print("Saving full resolution into "+newName)
tifffile.imwrite(newName, img)
newName = os.path.basename(os.path.splitext(str(sys.argv[1]))[0])+"_div_reduced.tif"
print("Saving 32 bit float into "+newName)
tifffile.imwrite(newName, img.astype(np.float32))
