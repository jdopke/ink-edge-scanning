## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

import cv2

import numpy as np
import struct
import os
import sys
import tifffile
from DataReader import readData

if (len(sys.argv)<2):
    print("To overlay a source image with coloured signal regions, call with source image as parameter:\n   "+str(sys.argv[0])+" <IMAGE>")
    sys.exit()

img = readData(str(sys.argv[1]))
img = (img-np.min(img))/(np.max(img)-np.min(img))
newName = os.path.splitext(sys.argv[1])[0]+"_scaled.tif"
tifffile.imwrite(newName, img.astype(np.float32))
