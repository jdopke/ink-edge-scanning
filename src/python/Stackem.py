## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

# import cv2

import numpy as np
import struct
import os
import sys
import tifffile

if (len(sys.argv) < 2):
	print("Need the source directory to stack up as an argument: "+str(sys.argv[0])+" <DIRECTORY>")
	sys.exit()

names = [os.path.join(str(sys.argv[1]), f) for f in os.listdir(str(sys.argv[1]))]

directory = os.path.abspath(r".")+str("/Stacked")

print(directory) 


if not os.path.exists(directory):
	os.makedirs(directory)

#just generating a buffer
buffer = []
filename = ""
nstacked = 0

image_sequence = (tifffile.imread([i for i in names if i.endswith('.tif')])).astype(np.float64)

print(image_sequence.shape)

buffer = np.sum(image_sequence, axis=0)

img = buffer.astype(np.float32)
newName = os.path.abspath(os.path.dirname(i))+"/Stacked/Stacked.tif"
print("Saving into "+newName)
tifffile.imwrite(newName, img)
