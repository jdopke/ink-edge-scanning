## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

import cv2

import numpy as np
import struct
import os
import sys
import tifffile
from DataReader import readData

def drawZone(img, pointData, colour):
    start_point = (pointData[0], pointData[1])
    end_point = (pointData[2], pointData[3])
    thickness = 2
    img = cv2.rectangle(img, start_point, end_point, colour, thickness)
    return img

#Region definitions
Alpha = [300,150,400,300]
Beta = [640,150,750,270]
Beta_3 = [1040,140,1140,270]

Background = [900,150,1000,270]

if (len(sys.argv)<2):
    print("To overlay a source image with coloured signal regions, call with source image as parameter:\n   "+str(sys.argv[0])+" <IMAGE>")
    sys.exit()

img = readData(str(sys.argv[1]))
if (len(img.shape) < 3):
    img = cv2.cvtColour(img, cv.COLOR_GRAY2BGR)

colour = (0,0,255)
img = drawZone(img, Alpha, colour)
img = drawZone(img, Beta, colour)
img = drawZone(img, Beta_3, colour)
colour = (0,255,0)
img = drawZone(img, Background, colour)

filename = r"./"+os.path.splitext(os.path.basename(sys.argv[1]))[0]+".jpg"
cv2.imwrite(filename, img)
