import numpy as np
import struct
import os
import sys
import tifffile

from DataReader import readImg

def readWrite(inFileName, outDirectory):
    newName = os.path.basename(os.path.splitext(str(inFileName))[0])+".tif"
    newName = os.path.join(outDirectory, os.path.basename(newName))
    img = readImg(str(inFileName))
    #img = (img - np.min(img))/(np.max(img) - np.min(img))
    print("Saving 32 bit float into "+newName)
    tifffile.imwrite(newName, img.astype(np.float32))
    

if (len(sys.argv)<2):
    print("To use, give me a parameter (filename):\n   "+str(sys.argv[0])+" <IMAGE>")
    print("Advanced use: ITEXConvertor.py <Input> <TargetDir>")
    sys.exit()

directory = ""
if (len(sys.argv) >2):
    directory = os.path.abspath(sys.argv[2])
    if not os.path.exists(directory):
        os.makedirs(directory)

scandirectory = os.path.isdir(sys.argv[1])
if (scandirectory & (len(sys.argv) <3)):
    print("Need an output directory to process input directory")
    sys.exit()

if scandirectory:
    names = [os.path.join(str(sys.argv[1]), f) for f in os.listdir(str(sys.argv[1]))]
    names = [i for i in names if i.endswith('.img')]
    for i in names:
        readWrite(i, directory)
else:
    readWrite(sys.argv[1], directory)



#print(newName)
