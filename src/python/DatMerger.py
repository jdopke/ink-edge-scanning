## Image stacker for CT scan recordings and the like at Diamond Beamline B16
## requires numpy, opencv, tifffile and imagecodecs to be installed results files are 12Bit compressed tiff, hence the latest package is needed tifffile plus imagecodecs can do the 
## decoding If called for stacking where a Stacked image already exists, that one is removed and the program exits

# import cv2

import numpy as np
import struct
import os
import sys
import tifffile
from DataReader import readData

if (len(sys.argv) < 2):
    print("Need at least a source directory to work with: "+str(sys.argv[0])+" <SOURCEDIRECTORY> [<TARGETDIRECTORY> [<NFILES> [<SKIPFILES>]]]")
    sys.exit()


sourceDir = str(sys.argv[1])
targetDir = r"."

targetNum = -1
dropNum = 0

if (len(sys.argv) >2):
    targetDir = str(sys.argv[2])
    if (len(sys.argv)>3):
        targetNum = int(sys.argv[3])
        print("Trying to reach "+str(targetNum)+" files")
        if (len(sys.argv)>4):
            dropNum = int(sys.argv[4])
            print("Trying to skip "+str(dropNum)+" files")

if (targetNum == 0):
    print("Exiting as you told me to stack zero files")
    sys.exit()

names = [os.path.join(sourceDir, f) for f in os.listdir(sourceDir)]
names.sort()

buffer = []
nStacked = 0

firstFile = ""
countFiles = 0

for i in names:
    if (".tif" in i) or (".dat" in i):
        countFiles = countFiles+1
        if not (countFiles >= dropNum):
            continue
        if (countFiles == dropNum):
            print ("Dropped "+str(countFiles)+" Files, now accumulating")
        if (len(firstFile) < 1):
            firstFile = i
        img = readData(i)
        if (len(buffer) == 0):
            buffer = img
            nStacked = nStacked + 1
        else:
            if (buffer.shape != img.shape):
                print("Shapes of file "+i+" and previous reads do not match, shapes are:")
                print("Previous Buffer: "+str(buffer.shape))
                print("Current File:    "+str(img.shape))
            buffer += img
            nStacked+=1
        print("{:2d} Files stacked".format(nStacked), end='\r')
    if (nStacked == targetNum):
        break
print("")
directory = os.path.abspath(targetDir)
if not os.path.exists(directory):
    os.makedirs(directory)

targetFile = os.path.join(directory, os.path.basename(firstFile))

if (nStacked != targetNum) and (targetNum != -1):
    print("Could not stack enough sourcefiles!")
np.savetxt(targetFile, buffer)
newName = os.path.splitext(targetFile)[0]+".tif"
print("Generating float64 output image: "+newName)
tifffile.imwrite(newName, buffer)
newName = os.path.splitext(targetFile)[0]+"_reduced.tif"
print("Generating float32 output image: "+newName)
tifffile.imwrite(newName, buffer.astype(np.float32))
newName = os.path.splitext(targetFile)[0]+"_reduced_scaled.tif"
buffer = (buffer-np.min(buffer)+0.001)/(np.max(buffer)+0.001-np.min(buffer))
print("Generating float32 output image (scaled 0-1): "+newName)
tifffile.imwrite(newName, buffer.astype(np.float32))

