import pillow_jpls
from PIL import Image
import tifffile
import numpy as np
import sys
import os

if (len(sys.argv)<2):
	print("Need a file to compress as parameter!")
	sys.exit()

img = tifffile.imread(sys.argv[1])


filename = os.path.splitext(os.path.basename(sys.argv[1]))[0]+".jls"
Image.fromarray(np.asarray(img), "I;16").save(filename, format="JPEG-LS", bits_per_sample=16)
