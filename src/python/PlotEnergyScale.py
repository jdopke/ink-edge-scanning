import os
import sys
import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(sys.argv[1])

print(data.shape)

plt.plot(data[::2,0], data[::2,1], 'k*')
plt.fill_between(data[::2,0], data[::2,1]-data[::2,2], data[::2,1]+data[::2,2])
plt.plot(data[::2,0], data[::2,3], 'k-')
plt.fill_between(data[::2,0], data[::2,3]-data[::2,4], data[::2,3]+data[::2,4])
plt.plot(data[::2,0], data[::2,5], 'k-')
plt.fill_between(data[::2,0], data[::2,5]-data[::2,6], data[::2,5]+data[::2,6])
plt.plot(data[::2,0], data[::2,7], 'k-')
plt.fill_between(data[::2,0], data[::2,7]-data[::2,8], data[::2,7]+data[::2,8])

plt.show()

alphaScale = np.zeros([data.shape[0], data.shape[0]])
betaScale = np.zeros([data.shape[0], data.shape[0]])
beta3Scale = np.zeros([data.shape[0], data.shape[0]])

for i in range(data.shape[0]):
    for j in range(data.shape[0]):
        alphaScale[i,j] = data[i,3]/data[j,3]
        betaScale[i,j] = data[i,5]/data[j,5]
        beta3Scale[i,j] = data[i,7]/data[j,7]
        
print("About to show you the matrices")
plt.matshow(alphaScale)
plt.show()
plt.matshow(betaScale)
plt.show()
plt.matshow(beta3Scale)
plt.show()