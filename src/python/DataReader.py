# No cv2 at CLS
#import cv2

import numpy as np
import tifffile
import os
import sys
import struct

# Python script to read HiPic img file
# K.Tamasaku 6/24/2020

# # Sample program for read_img
# import matplotlib.pyplot as plt
#
# data=(read_img('test.img'))[700:1500, 500:1500] # ROI
# fig=plt.subplots()
# plt.imshow(data)
# plt.show()
#

def readImg(fn: str) -> list:
    try:
        f=open(fn, "rb")
    except OSError as e:
        print(e)
    else:
        imgdata=f.read()
        fid, cmtlen = struct.unpack_from("<2sh", imgdata, 0) # little endian
        if fid != b'IM':
            print(f'{fn} is not HiPic img format.')
            return None
        else:
            print(f'Reading file with comment length: {cmtlen}')
        xsize, ysize, xoffset, yoffset, datatype=struct.unpack_from("<hhhhh", imgdata, 4)
        print("Size (X/Y):"+str(xsize)+"/"+str(ysize))
        print("Offset (X/Y):"+str(xoffset)+"/"+str(yoffset))
        #print(fid, cmtlen, xsize, ysize)
        comment=struct.unpack_from(f'<{cmtlen}s', imgdata, 64)
        #print(comment)
        typecharacter = "H"
        if (datatype == 0):
            print("Reading 8bit data")
            typecharacter = "B"
        elif (datatype == 3):
            print("Reading 32bit data")
            typecharacter = "I"
        else:
            print("Reading 16bit data, datatype was:"+str(datatype))
        filesize = xsize*ysize
        data=(np.array(struct.unpack_from("<"+typecharacter*filesize, imgdata, 64+cmtlen))).reshape([ysize, xsize]).astype(np.float64)
        f.close()

        return data


def readData(fileName):
    buffer = []
#    print(os.path.splitext(fileName))
    if (os.path.splitext(fileName)[1]==".dat"):
        buffer = np.loadtxt(fileName).astype(np.float64)
#    elif (os.path.splitext(fileName)[1]==".jpg") or (os.path.splitext(fileName)[1]==".jpeg"):
#        buffer = cv2.imread(fileName)
    elif (os.path.splitext(fileName)[1]==".tif") or (os.path.splitext(fileName)[1]==".tiff"):
        buffer = tifffile.imread(fileName).astype(np.float64)
    else :
        buffer = readImg(fileName)
    return buffer
    

