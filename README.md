# Ink Edge Scanning

Python and C++ code related to x-ray edge scanning of inks on papyrus.

## Getting started

Code is delivered in the *src* folder, with separate subfolders for *python* and C++ (*cpp*) related code.

## Python parts, a quick wrapup

Python code has elvolved from code used previously into data taking and quick understanding, as well as later tools for conversion. Heart of the code is DataReader.py, which contains (currently) two read functions:

```python
def readData(fileName)

def readImg(fileName)
```

Both functions take a single filename as input and will return a numpy array (or none) in shape of the image data read back, typed as float64 for calculations.

When writing out files, note that `tifffile` **can** write `float64` tiffs, but **pretty much nothing else can read it**.

